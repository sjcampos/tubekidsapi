const User = require('../models/user.model.js');
const crypto = require('crypto');
const API = require('../../secrets/sendgrid.json');

// Create and Save a new user
exports.create = (req, res) => {
        // Validate request
        switch (validateData(req)) {
            case 1:
                return res.status(400).send({
                    message: "Please fill all required fields"
                });

            case 2:
                return res.status(400).send({
                    message: "Please fill all required fields"
                });

            case 3:
                return res.status(400).send({
                    message: "Empty fields are not allowed"
                });
            case 4:
                return res.status(400).send({
                    message: "Enter a valid email"
                });

            case 5:
                return res.status(400).send({
                    message: "Enter a valid date"
                });

            case 6:
                return res.status(400).send({
                    message: "The person in charge of the account must be of legal age"
                });
            case 7:
                return res.status(400).send({
                    message: "Passwords do not match"
                });
            case 8:
                // Create a new user
                const user = new User({
                    name: req.body.name,
                    last_name: req.body.last_name,
                    country: req.body.country,
                    phone: req.body.phone,
                    birthday: req.body.birthday,
                    email: req.body.email,
                    password: encryptPassword(req.body.name, req.body.password),
                    registrationcode: generateReCode(req.body.name),
                    verificationcode: 'generic',
                    active: false
                });
                //Sends the email with the verification code
                const sgMail = require('@sendgrid/mail');
                sgMail.setApiKey(API['SENDGRID_API_KEY']);
                const msg = {
                    to: user.email,
                    from: 'tubekidsproject@gmail.com',
                    subject: 'Activate your account in TubeKids Project',
                    html: `<strong>Hello ${user.name}!!!<br> Here is your verification code: ${user.registrationcode}<br>Hope you enjoy our services.</strong>`,
                };
                //ES6
                sgMail
                    .send(msg)
                    .then(() => {}, console.error);
                //ES8
                // (async() => {
                //     try {
                //         await sgMail.send(msg);
                //     } catch (err) {
                //         console.error(err.toString());
                //     }
                // })();
                // Save a user in the database
                user.save()
                    .then(data => {
                        res.send(data);
                    }).catch(err => {
                        res.status(500).send({
                            message: err.message || "Something went wrong while creating new register."
                        });
                    });

        }
    }
    //Generates the registration code
function generateReCode(name) {
    var namearray = name.split("");
    var rcode = namearray[0];
    while (rcode.length < 6) {
        rcode = Math.floor(Math.random() * (9 - 1)) + 1 + rcode;
    }
    return rcode;
}
//Encrypts the password
function encryptPassword(name, pass) {
    var hmac = crypto.createHmac('sha1', name).update(pass).digest('hex');
    return hmac;
}
//Checks that the email is real
function validateEmail(valor) {
    var patron = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    if (valor.search(patron) == 0) {
        return false;
    } else {
        return true;
    }
}
//Checks if the date is valid and real
function isValidDate(day, month, year) {
    var dteDate;
    month = month - 1;
    dteDate = new Date(year, month, day);
    return ((day == dteDate.getDate()) && (month == dteDate.getMonth()) && (year == dteDate.getFullYear()));
}
//Checks if the data from the date is valid
function validateDate(d) {
    if (Object.entries(d).length === 0) {
        return true;
    } else {
        var valuesDate = d.split("/");
        if (valuesDate.length != 3) {
            return true;
        }
        if ((valuesDate[2].length != 2) || (valuesDate[1].length != 2) || (valuesDate[0].length != 4)) {
            return true;
        }
        if (isValidDate(valuesDate[2], valuesDate[1], valuesDate[0])) {
            return false;
        } else {

            return true;
        }
    }

}
//Validates if the person is of legal age
function validateAge(d) {
    var values = d.split("/");
    var day = values[2];
    var month = values[1];
    var year = values[0];

    var fecha_hoy = new Date();
    var actual_year = fecha_hoy.getYear();
    var actual_month = fecha_hoy.getMonth() + 1;
    var actual_day = fecha_hoy.getDate();

    var age = (actual_year + 1900) - year;
    if (actual_month < month) {
        age--;
    }
    if ((month == actual_month) && (actual_day < day)) {
        age--;
    }
    if (age > 1900) {
        age -= 1900;
    }
    if (age < 18) {
        return true;
    } else {
        return false;
    }

}
//Checks if the data is valid
function validateData(v) {
    if (Object.entries(v.body).length === 0) {
        return 1;
    }
    if (v.body.name == "" || v.body.last_name == "" || v.body.country == "" || v.body.phone == "" ||
        v.body.phone == "" || v.body.birthday == "" || v.body.email == "" || v.body.pass == "" ||
        v.body.repass == "") {
        return 2;
    }
    if (v.body.name.trim().length == 0 || v.body.last_name.trim().length == 0 || v.body.country.trim().length == 0 ||
        v.body.phone.trim().length == 0 || v.body.birthday.trim().length == 0 || v.body.email.trim().length == 0 ||
        v.body.password.trim().length == 0 || v.body.repass.trim().length == 0) {
        return 3;
    }
    if (validateDate(v.body.birthday)) {
        return 5;
    }
    if (validateAge(v.body.birthday)) {
        return 6;
    }
    if (validateEmail(v.body.email)) {
        return 4;
    }
    if (v.body.password != v.body.repass) {
        return 7;
    } else {
        return 8;
    }

}