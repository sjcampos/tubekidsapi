const Video = require('../models/videos.model.js');
const newvideo = require('../models/newvideo.model');
// Retrieve and return all videos from the database.
exports.findByIdUpdate = (req, res) => {
    id = req.params.id;
    Video.findById(id)
        .then(videos => {
            res.send(videos);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something went wrong while getting your video."
            });
        });
};
// Create and Save a new video
exports.create = (req, res) => {
    // Validate request
    switch (validateData(req)) {
        case 1:
            return res.status(400).send({
                message: "Please fill all required fields"
            });

        case 2:
            return res.status(400).send({
                message: "Please fill all required fields"
            });

        case 3:
            return res.status(400).send({
                message: "Enter a valid youtube url"
            });
        case 4:
            return res.status(400).send({
                message: "Empty fields are not allowed"
            });
        case 5:
            // Create a new video
            const video = new Video({
                id: "",
                id_user: req.body.id_user,
                video_name: req.body.video_name,
                url: getValidUrl(req.body.url),

            });
            // Save a video in the database
            video.save()
                .then(data => {
                    res.send(data);
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Something went wrong while creating new video."
                    });
                });

    }
}

// Find all the videos with the same id
exports.findAllByID = (req, res) => {
    id_user = req.params.id;
    Video.find({ id_user })
        .then(video => {
            if (!video) {
                return res.status(404).send({
                    message: "Video not found with id " + req.params.id
                });
            }
            res.send(video);
        }).catch(err => {
            console.log(err);
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Video not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Error getting video with id " + req.params.id
            });
        });
};
// Update a Video identified by the id in the request
exports.update = (req, res) => {
    // Validate Request
    switch (validateData(req)) {
        case 1:
            return res.status(400).send({
                message: "Please fill all required fields"
            });
            break;
        case 2:
            return res.status(400).send({
                message: "Please fill all required fields"
            });
            break;
        case 3:
            return res.status(400).send({
                message: "Enter a valid youtube url"
            });

        case 4:
            return res.status(400).send({
                message: "Empty fields are not allowed"
            });
        case 5:
            // Find video and update it with the request body
            Video.findByIdAndUpdate(req.params.id, {
                    id_user: req.body.id_user,
                    video_name: req.body.video_name,
                    url: req.body.url,
                }, { new: true })
                .then(video => {
                    if (!video) {
                        return res.status(404).send({
                            message: "Video not found with id " + req.params.id
                        });
                    }
                    res.send(video);
                }).catch(err => {
                    if (err.kind === 'ObjectId') {
                        return res.status(404).send({
                            message: "Video not found with id " + req.params.id
                        });
                    }
                    return res.status(500).send({
                        message: "Error updating video with id " + req.params.id
                    });
                });
    }
};
// Delete a Video with the specified id in the request
exports.delete = (req, res) => {
    Video.findByIdAndRemove(req.params.id)
        .then(video => {
            if (!video) {
                return res.status(404).send({
                    message: "Video not found with id " + req.params.id
                });
            }
            res.send({ message: "Video deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Video not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete video with id " + req.params.id
            });
        });
};

//This function checks if the url is valid
function matchYoutubeUrl(url) {
    var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    if (url.match(p)) {
        return url.match(p)[1];
    }
    return false;
}

//This function checks if the data is valid
function validateData(v) {
    if (Object.entries(v.body).length === 0) {
        return 1;
    }
    if (v.body.id_user == "" || v.body.video_name == "" || v.body.url == "") {
        return 2;
    }
    if (!matchYoutubeUrl(v.body.url)) {
        return 3;
    }
    if (v.body.id_user.trim().length == 0 || v.body.video_name.trim().length == 0 || v.body.url.trim().length == 0) {
        return 4;
    } else {
        return 5;
    }
}

function getValidUrl(oldurl) {
    newurl = "https://www.youtube.com/embed/";
    old = oldurl.split("=");
    newurl = newurl + old[1];
    console.log(newurl);
    return newurl;

}