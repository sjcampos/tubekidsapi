const Profile = require('../models/profiles.model');

// Retrieve and return all the profiles from the database.
exports.findAll = (req, res) => {
    id_father = req.params.id;
    Profile.find({ id_father })
        .then(profiles => {
            res.send(profiles);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something went wrong while getting your list of profiles."
            });
        });
};

//Gets one profile information with an specific id
exports.findOne = (req, res) => {
    id = req.params.id;
    Profile.findById(id)
        .then(profiles => {
            res.send(profiles);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something went wrong while the profile information."
            });
        });
};

// Create and Save a new profile
exports.create = (req, res) => {
    // Validate request
    switch (validateData(req)) {
        case 1:
            return res.status(400).send({
                message: "Please fill all required fields"
            });

        case 2:
            return res.status(400).send({
                message: "Please fill all required fields"
            });

        case 3:
            return res.status(400).send({
                message: "The pin must have 6 digits"
            });
        case 4:
            return res.status(400).send({
                message: "Empty fields are not allowed"
            });
        case 5:
            // Create a new profile
            const profile = new Profile({
                id: "",
                id_father: req.body.id_father,
                name: req.body.name,
                user_name: req.body.user_name,
                pin: req.body.pin,
                age: req.body.age

            });

            // Save a profile in the database
            profile.save()
                .then(data => {
                    res.send(data);
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Something went wrong while creating new profile."
                    });
                });

    }
}

// Update a Video identified by the id in the request
exports.update = (req, res) => {
    // Validate Request
    switch (validateData(req)) {
        case 1:
            return res.status(400).send({
                message: "Please fill all required fields"
            });

        case 2:
            return res.status(400).send({
                message: "Please fill all required fields"
            });

        case 3:
            return res.status(400).send({
                message: "The pin must have 6 digits"
            });
        case 4:
            return res.status(400).send({
                message: "Empty fields are not allowed"
            });
        case 5:
            // Find a profile and update it with the request body
            Profile.findByIdAndUpdate(req.params.id, {
                    id_father: req.body.id_father,
                    name: req.body.name,
                    user_name: req.body.user_name,
                    pin: req.body.pin,
                    age: req.body.age
                }, { new: true })
                .then(video => {
                    if (!video) {
                        return res.status(404).send({
                            message: "Profile not found with id " + req.params.id
                        });
                    }
                    res.send(video);
                }).catch(err => {
                    if (err.kind === 'ObjectId') {
                        return res.status(404).send({
                            message: "Profile not found with id " + req.params.id
                        });
                    }
                    return res.status(500).send({
                        message: "Error updating profile with id " + req.params.id
                    });
                });
    }
};
// Delete a profile with the specified id in the request
exports.delete = (req, res) => {
    Profile.findByIdAndRemove(req.params.id)
        .then(profile => {
            if (!profile) {
                return res.status(404).send({
                    message: "Profile not found with id " + req.params.id
                });
            }
            res.send({ message: "Profile deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Profile not found with id " + req.params.id
                });
            }
            return res.status(500).send({
                message: "Could not delete video with id " + req.params.id
            });
        });
};

//This function checks if the data is valid
function validateData(child) {
    if (Object.entries(child.body).length === 0) {
        return 1;
    }
    if (child.body.id_user == "" || child.body.name == "" || child.body.user_name == "" || child.body.pin == "" || child.body.age == "") {
        return 2;
    }
    if (child.body.pin.length < 6 || child.body.pin.length > 6) {
        return 3;
    }
    if (child.body.id_father.trim().length == 0 || child.body.name.trim().length == 0 || child.body.user_name.trim().length == 0 ||
        child.body.pin.trim().length == 0 || child.body.age.trim().length == 0) {
        return 4;
    } else {
        return 5;
    }
}