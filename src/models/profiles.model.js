const mongoose = require('mongoose');

const ProfilesSchema = mongoose.Schema({
    id: String,
    id_father: String,
    name: String,
    user_name: String,
    pin: String,
    age: String,
    is_active: { type: Boolean, default: false },
    is_verified: { type: Boolean, default: false },
    is_deleted: { type: Boolean, default: false }
}, {
    timestamps: true
});

module.exports = mongoose.model('Profiles', ProfilesSchema);