const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name: String,
    last_name: String,
    country: String,
    phone: String,
    birthday: Date,
    email: String,
    password: String,
    repass: String,
    registrationcode: String,
    verificationcode: String,
    active: Boolean,
    is_active: { type: Boolean, default: false },
    is_verified: { type: Boolean, default: false },
    is_deleted: { type: Boolean, default: false }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);