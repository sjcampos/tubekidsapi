const mongoose = require('mongoose');

const NewvideoSchema = mongoose.Schema({
    id: String,
    id_user: String,
    video_name: String,
    url: String,
    is_active: { type: Boolean, default: false },
    is_verified: { type: Boolean, default: false },
    is_deleted: { type: Boolean, default: false }
}, {
    timestamps: true
});

module.exports = mongoose.model('newVideo', NewvideoSchema);