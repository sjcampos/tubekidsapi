const express = require('express')
const router = express.Router()
const videoController = require('../controllers/videos.controllers');

// Gets a video with the same id
router.get('/videodata/:id', videoController.findByIdUpdate);
// Create a new video
router.post('/', videoController.create);
// Get a single video with id
router.get('/:id', videoController.findAllByID);
// Update a video with id
router.put('/:id', videoController.update);
// Delete a video with id
router.delete('/:id', videoController.delete);
module.exports = router