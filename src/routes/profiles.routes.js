const express = require('express')
const router = express.Router()
const profileController = require('../controllers/profiles.controllers');

// Gets all the profiles
router.get('/:id', profileController.findAll);
// Create a new profile
router.post('/', profileController.create);
// Get a single profile with id
router.get('/profiledata/:id', profileController.findOne);
// Update a profile with id
router.put('/:id', profileController.update);
// Delete a profile with id
router.delete('/:id', profileController.delete);
module.exports = router