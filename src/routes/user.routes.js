const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.controllers');


// Create a new user
router.post('/', userController.create);
// Get a single user with id (login)
//router.get('/:id', userController.findOne);
//Preguntar
// Update a video with id
//router.put('/:id', userController.update);
// Delete a video with id
//router.delete('/:id', userController.delete);
module.exports = router