const User = require('../models/user.model');
const Profile = require('../models/profiles.model');
const crypto = require('crypto');
const twiliokeys = require('../../secrets/Twilio.json');
const client = require('twilio')(twiliokeys['accountSid'], twiliokeys['authToken']);
const jwt = require("jsonwebtoken");
const jwtsecret = require('../../secrets/jwt.json');
const theSecretKey = jwtsecret['Secret'];

//This the function that activates the user account
function registration(req, res) {
    var registrationcode = req.body.registrationcode;
    //Checks if there is an user with that registration code
    User.findOne({ registrationcode })
        .then(user => {
            if (!user) {
                return res.status(400).send({
                    message: "That is not a valid registration code"
                });
            }
            //If the user is not active the server updates the user if everything its fine
            if (!user.active) {
                User.findByIdAndUpdate(user.id, {
                        name: user.name,
                        last_name: user.last_name,
                        country: user.country,
                        phone: user.phone,
                        birthday: user.birthday,
                        email: user.email,
                        password: user.password,
                        registrationcode: user.registrationcode,
                        verificationcode: user.verificationcode,
                        active: true,
                    }, { new: true })
                    .then(user => {
                        if (!user) {
                            return res.status(404).send({
                                message: "Ups! That is not a valid code, please try again!"
                            });
                        }

                        res.send(user);
                    }).catch(err => {
                        if (err.kind === 'ObjectId') {
                            return res.status(404).send({
                                message: "Ups! That is not a valid code, please try again!"
                            });
                        }

                        return res.status(500).send({
                            message: "Ups! There was an error with our server, please try again!"
                        });
                    });
            }
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "There was an error while activating your account, try again. "
                });
            }
            return res.status(500).send({
                message: "There was an error getting your data, try again."
            });
        });
};
//Here is the function that validates the login information and sends the sms with the verification code
function authuser(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    User.findOne({ email })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "Username or password are incorrect"
                });
            }
            if (!user.active) {
                return res.status(400).send({
                    message: "001"
                });
            }
            if (user.password == crypto.createHmac('sha1', user.name).update(password).digest('hex')) {
                //send sms
                var twofactor = "";
                while (twofactor.length < 6) {
                    twofactor = Math.floor(Math.random() * (9 - 1)) + 1 + twofactor;
                }
                User.findByIdAndUpdate(user.id, {
                        name: user.name,
                        last_name: user.last_name,
                        country: user.country,
                        phone: user.phone,
                        birthday: user.birthday,
                        email: user.email,
                        password: user.password,
                        registrationcode: user.registrationcode,
                        verificationcode: twofactor,
                        active: user.active,
                    }, { new: true })
                    .then(user => {
                        if (!user) {
                            return res.status(404).send({
                                message: "Ups! Invalid data, please try again!"
                            });
                        }
                        //This creates the message and send it to the user
                        // client.messages
                        //     .create({
                        //         body: `This is your verification code: ${twofactor}`,
                        //         from: '+12058392657',
                        //         to: `+506${user.phone}`
                        //     })
                        //     .then(message => console.log(message.sid));
                        console.log('TwoFactor code', user.verificationcode);
                        return res.status(201).send({
                            message: "002"
                        });
                    }).catch(err => {
                        if (err.kind === 'ObjectId') {
                            return res.status(404).send({
                                message: "Ups! Invalid data, please try again!"
                            });
                        }
                        console.log("Aqui");
                        return res.status(500).send({
                            message: "Ups! There was an error with our server, please try again!"
                        });
                    });

            } else {
                return res.status(404).send({
                    message: "Ups! Invalid data, please try again!"
                });
            }
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "There was an error with your authentication "
                });
            }
            return res.status(404).send({
                message: "Ups! Invalid data, please try again!"
            });
        });
};
//This function creates the Json Web token and creates a new session for the parents
function session(req, res, next) {
    var verificationcode = req.body.verificationcode;
    User.findOne({ verificationcode })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "Try again"
                });
            }
            if (!user.active) {
                return res.status(404).send({
                    message: "001"
                });
            }
            if (user.verificationcode === verificationcode) {
                //var validDate = new Date();
                var created = new Date();
                //validDate.setHours(validDate.getHours() + 1);
                const token = jwt.sign({
                    userId: user.id,
                    name: user.name,
                    permission: ['create', 'edit', 'delete'],
                    created: created.getHours() * 3,
                    expire: (created.getHours() + 1) * 5,

                }, theSecretKey);

                res.status(201).json({
                    token
                })
            } else {
                next();
            }

        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "There was an error with your authentication "
                });
            }
            console.log(err);
            return res.status(500).send({
                message: "There was an error getting your data"
            });
        });

};
//Validates if the token is valid
function validsession(req, res, next) {
    if (req.headers["authorization"]) {
        const authToken = req.headers['authorization'].split(' ')[1];
        try {
            jwt.verify(authToken, theSecretKey, (err, decodedToken) => {
                if (err || !decodedToken) {

                    res.status(401);
                    res.json({
                        error: "Unauthorized"
                    });
                }
                console.log(decodedToken);
                //Checks if the token es valid
                if (decodedToken.fatherId != null) {

                    Profile.findById(decodedToken.profileId)
                        .then(profile => {
                            if (!profile) {
                                return res.status(404).send({
                                    message: "Unauthorized"
                                });
                            }
                            var actualDate = new Date();
                            if (decodedToken['permission'][0] == 'get') {
                                if (decodedToken['created'] / 3 == actualDate.getHours()) {
                                    if ((decodedToken['expire'] / 5 - decodedToken['created'] / 3) == 1) {
                                        next();
                                    }
                                }
                            } else {
                                return res.status(401).send({
                                    message: "Unauthorized"
                                });
                            }

                        }).catch(err => {
                            if (err.kind === 'ObjectId') {
                                return res.status(404).send({
                                    message: "Unauthorized"
                                });
                            }
                            return res.status(500).send({
                                message: "Error getting profile authentication"
                            });
                        });
                } else {

                    User.findById(decodedToken.userId)
                        .then(user => {
                            if (!user) {
                                return res.status(404).send({
                                    message: "Unauthorized"
                                });
                            } else {
                                var actualDate = new Date();
                                if (decodedToken['permission'][0] == 'create' & decodedToken['permission'][1] == 'edit' & decodedToken['permission'][2] == 'delete') {

                                    if (decodedToken['created'] / 3 == actualDate.getHours()) {
                                        console.log(decodedToken.userId);
                                        if ((decodedToken['expire'] / 5 - decodedToken['created'] / 3) == 1) {
                                            next();
                                        }
                                    }
                                } else {
                                    return res.status(401).send({
                                        message: "Unauthorized"
                                    });
                                }
                            }

                        }).catch(err => {
                            if (err.kind === 'ObjectId') {
                                return res.status(404).send({
                                    message: "Unauthorized"
                                });
                            }
                            return res.status(500).send({
                                message: "Error getting user authentication"
                            });
                        });
                }

            });
        } catch (e) {
            next();
        }


    } else {
        res.status(401);
        res.send({
            error: "Unauthorized "
        });
    }
};
//Creates a restrictedsession for the kids profile and checks that is a valid profile
//then returns a json web token for the profile
function restrictedsession(req, res) {
    var user_name = req.body.user_name;
    var pin = req.body.pin;
    Profile.findOne({ user_name })
        .then(profile => {
            if (!profile) {
                return res.status(400).send({
                    message: "The username or code are incorrect!!"
                });
            }

            if (profile.pin == pin) {
                //var validDate = new Date();
                var created = new Date();
                //validDate.setHours(validDate.getUTCHours() + 1);
                //console.log(validDate.getUTCHours());
                const token = jwt.sign({
                    profileId: profile._id,
                    fatherId: profile.id_father,
                    username: profile.user_name,
                    permission: ['get'],
                    created: created.getHours() * 3,
                    expire: (created.getHours() + 1) * 5,
                }, theSecretKey);

                res.status(201).json({
                    token
                })
            } else {
                return res.status(400).send({
                    message: "The username or code are incorrect!!"
                });
            }
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "There was an error with your authentication "
                });
            }
            return res.status(500).send({
                message: "There was an error getting your data"
            });
        });

}



module.exports = {
    authuser: authuser,
    registration: registration,
    session: session,
    validsession: validsession,
    restrictedsession: restrictedsession,

}