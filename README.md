**Tubekids API REST server**

This is the backend server for the tubekids project in the web programming class 2.

**It was built with  🛠️**

[Nodejs](https://nodejs.org/es/)- As framework

[MongoDB](https://www.mongodb.com/es)- DB

**Author ✒️**

Sebastián Campos 
