const express = require('express');
const bodyParser = require('body-parser');
const middleware = require('./src/middlewares/middlewares');
const cors = require("cors");

// create express app
const app = express();
// Setup server port
const port = process.env.PORT || 4000;
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
    // parse requests of content-type - application/json
app.use(bodyParser.json())
    // Configuring the database
const dbConfig = require('./config/db.config.js');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database.', err);
    process.exit();
});

// check for cors
app.use(cors());

//Activates the user account after the registration process
app.put("/api/activenew", function(req, res, next) {
    middleware.registration(req, res);
});
//Auth the kids profiles 
app.post("/api/restrictedsession", function(req, res, next) {
    middleware.restrictedsession(req, res);
});

//Auth the information and send the sms with the verification code
app.post("/api/authentic", function(req, res, next) {
    middleware.authuser(req, res);
});
//Create the json web token
app.post("/api/session", function(req, res, next) {
    middleware.session(req, res, next);
});

//This is the route to register new users, so it doesn't need to be validate by a token
const userRoutes = require('./src/routes/user.routes')
app.use('/api/users', userRoutes)

//Validates JWT 
app.use(function(req, res, next) {
    middleware.validsession(req, res, next);
});
// Require routes
const videosRoutes = require('./src/routes/videos.routes')
const profilesRoutes = require('./src/routes/profiles.routes')

app.use('/api/videos', videosRoutes)
app.use('/api/profiles', profilesRoutes)

// listen for requests
app.listen(port, () => {
    console.log(`Node server is listening on port ${port}`);
});